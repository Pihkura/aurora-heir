# Aurora Heir

Aurora Heir is a single player RTS / village builder / tower defense game which rests on Finnish folklore and mythology. Game is based on harvesting various resources and building up from handful of workers into a functional network of villages in which every resident has it's own role. Player may choose to micromanage everything or let workers do the decisions by assigning professions to villagers which will guide their actions and focus on higher level decisions.  

Build up your towers, recruit and train war band to protect your domain from various mythical creatures. Establish vital production with fishers, hunters, farmers, miners, woodcutters etc. Beware harsh Nordic winters, which may easily eat up your tribe leaving nothing more than frozen soil behind. Player may choose to embrace or defy ancient gods for decisions like this will determine the outcome of the game. Harness the power of your shamans for interaction with the gods.  

There are currently two tribes with different starting conditions and bonuses, which player may choose from. Game consists of five different classes (workers, shamans, warriors, scoundrels and rangers), 19 different professions, 25 different buildings + roads and paths and various critters and mythical creatures.  

Maps are randomly generated and contains bunch of various resources, critters and of course space for building your village. Choosing the most efficient starting point in terms of nearby resources and sacred grounds has it's advantages.  

# Issue tracker

Submit issues related to Aurora Heir by clicking *Issues* link on the left side panel. Please first search for open issues to see if someone has already submitted issue with same topic.
